Name:           gsound
Version:        1.0.3
Release:        1
Summary:        A small library for playing system sounds
License:        LGPLv2
URL:            https://wiki.gnome.org/Projects/GSound
Source0:        http://download.gnome.org/sources/gsound/1.0/gsound-%{version}.tar.xz

BuildRequires:  pkgconfig(gobject-introspection-1.0) pkgconfig(libcanberra) vala meson gtk-doc

%description
GSound is a small library for playing system sounds.
It's designed to be used via GObject Introspection,and is a thin wrapper around the libcanberra C library.

%package        devel
Summary:        Libraries and headers for gsound.
Requires:       %{name} = %{version}-%{release}

%description    devel
Libraries and header files for developing applications that use gsound.

%package_help

%prep
%autosetup -n gsound-%{version} -p1

%build
%meson -Dgtk_doc=true
%meson_build

%install
%meson_install
%delete_la

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%doc COPYING README
%{_bindir}/gsound-play
%{_libdir}/*.so.*
%{_libdir}/girepository-1.0/GSound-1.0.typelib

%files devel
%{_includedir}/*
%{_libdir}/*.so
%{_libdir}/pkgconfig/gsound.pc
%dir %{_datadir}/gir-1.0
%{_datadir}/gir-1.0/GSound-1.0.gir
%{_datadir}/vala/vapi/gsound.*

%files help
%{_mandir}/man1/gsound-play.*
%{_datadir}/gtk-doc/html/gsound-%{version}

%changelog
* Mon Mar 28 2022 lin zhang <lin.zhang@turbolinux.com.cn> - 1.0.3-1
- Update to 1.0.3

* Fri Dec 21 2019 chenli <chenli147@huawei.com> - 1.0.2-8
- Package init
